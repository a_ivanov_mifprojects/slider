import {Component, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {timer} from 'rxjs';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss'],
  animations: [
    trigger('fade', [
      state('in',
        style({opacity: 1})),

      transition(':enter', [
        style({opacity: 0}),
        animate(300)
      ]),
    ])
  ]
})
export class SliderComponent implements OnInit {

  images = [
    `https://picsum.photos/1280/720?image=${Math.floor(Math.random() * 100)}`,
    `https://picsum.photos/1280/720?image=${Math.floor(Math.random() * 100)}`,
    `https://picsum.photos/1280/720?image=${Math.floor(Math.random() * 100)}`,
    `https://picsum.photos/1280/720?image=${Math.floor(Math.random() * 100)}`,
    `https://picsum.photos/1280/720?image=${Math.floor(Math.random() * 100)}`
  ];

  curImage = 0;
  mouseOver: false;
  imagesLength;

  constructor() {
    timer(0, 2000).subscribe(t => {
      if (this.mouseOver) {
        return;
      }
      this.curImage += 1;
      this.changeImage(this.curImage);
    });
  }

  ngOnInit() {
    this.imagesLength = this.images.length - 1; // start from zero
  }

  prev() {
    this.curImage -= 1;
    this.changeImage(this.curImage);
  }

  next() {
    this.curImage += 1;
    this.changeImage(this.curImage);
  }

  changeImage(arrayKey) {
    if (arrayKey < 0) {
      this.curImage = this.imagesLength;
      return;
    }
    if (arrayKey > this.imagesLength) {
      this.curImage = 0;
      return;
    }
    this.curImage = arrayKey;
  }

  mouseState(bool) {
    this.mouseOver = bool;
  }
}
